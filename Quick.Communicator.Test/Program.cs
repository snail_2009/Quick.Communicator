﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quick.Communicator;
using System.Net;
using System.Threading;

namespace Quick.Communicator.Test
{
    class Program
    {
        static SuperSerialPort ssp = null;

        static void Main(string[] args)
        {
            //IPEndPoint ip = new IPEndPoint(IPAddress.Parse("localhost"), 6379);

            //SerialConfig sc = new SerialConfig();
            //sc.PortName = "COM10";
            //sc.BaudRate = 9600;
            //sc.DataBits = 8;
            //sc.DTR = false;
            //sc.RTS = false;
            //sc.StopBits = System.IO.Ports.StopBits.One;
            //sc.Parity = System.IO.Ports.Parity.None;

            //string scStr = sc.ToString();

            //ssp = new SuperSerialPort(sc, new DatagramResolver(Encoding.ASCII, "", true));
            //ssp.ReceiveDataEnd += Ssp_ReceiveDataEnd;
            //ssp.Start();
            //Console.Read();


            //SerialConfig sc2 = new SerialConfig(scStr);


            //IPAddress[] ips = Dns.GetHostAddresses("localhost");

            //TCPClient tcpClient = new TCPClient(new DatagramResolver("utf-8", "7E", true));
            //tcpClient.ReceiveDataEnd += TcpClient_ReceiveDataEnd;
            //tcpClient.Connect("localhost", 10086);
            //tcpClient.Close();
            //Console.ReadLine();

            //TCPServer tcpServer = new TCPServer(10086, 1, new DatagramResolver("utf-8", "7E", true));
            //tcpServer.ReceiveDataEnd += TcpServer_ReceiveDataEnd;
            //tcpServer.ClientClose += TcpServer_ClientClose;
            //tcpServer.Start();
            //tcpServer.Stop();
            //Console.ReadLine();
            ////tcpServer.Stop();
            //Console.ReadLine();

            //SuperSerialPort sp = new SuperSerialPort("COM1", 9600, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.None, new DatagramResolver("utf-8"));
            //sp.ReceiveDataEnd += Sp_ReceiveDataEnd;
            //sp.Start();
            //sp.Stop();
            //Console.ReadLine();

            //TCPServer tcpServer = new TCPServer(10086, 1, new DatagramResolver("utf-8", "7E", true));
            //tcpServer.ReceiveDataEnd += TcpServer_ReceiveDataEnd;
            //tcpServer.Start();
            //tcpServer.Stop();
            //Console.ReadLine();

            //UDPServer udpServer = new UDPServer(10086, 1, new JsonDatagramResolver("utf-8", "7E", true));
            //udpServer.ReceiveDataEnd += UdpServer_ReceiveDataEnd;
            //udpServer.Start();
            //udpServer.Stop();
            //Console.ReadLine();

            TCPClient tcpClient = new TCPClient(new DatagramResolver("utf-8"));
            tcpClient.ReceiveDataEnd += TcpClient_ReceiveDataEnd;
            while (true)
            {
                if (tcpClient.Connect("127.0.0.1", 7701))
                {
                    tcpClient.AsyncSend("1234567890");
                }
                Console.WriteLine("链接完成");
            }
            
            Console.ReadLine();

            tcpClient.Close();
            //UDPClient udpClient = new UDPClient(new DatagramResolver("utf-8"));
            //udpClient.ReceiveDataEnd += UdpClient_ReceiveDataEnd;
            //udpClient.Connect("127.0.0.1", 10086);
            //Console.ReadLine();

            /*
            WebSocket webSocket = new WebSocket(5555);
            webSocket.Resolver = new DatagramResolver("utf-8");
            webSocket.ClientConn += WebSocket_ClientConn;
            webSocket.ClientClose += WebSocket_ClientClose;
            webSocket.ReceiveDataEnd += WebSocket_ReceiveDataEnd;
            webSocket.Start();
            string send = "";
            while (true)
            {
                send = Console.ReadLine();
                webSocket.AsyncSend(m_lastUser,send);//发送信息给最后一个链接对象
            }
            */
        }

        private static void TcpServer_ClientClose(object sender, NetEventArgs e)
        {
            Console.WriteLine(e.Client.ID.ToString() +"离线");
        }

        private static void Ssp_ReceiveDataEnd(object obj, SerialPortEventArgs e)
        {
            List<Byte> list = new List<byte>();

            string[] s= e.Datagram.Split(new char[] { ' ' });

            for (int i = 0; i < s.Length; i++)
            {
                list.Add(Convert.ToByte(s[i]));
            }

            string s1 = Encoding.ASCII.GetString(list.ToArray());

            string[] s2 = s1.Split(new char[] {'\n', '\r' },StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < s2.Length; i++)
            {
                Console.WriteLine(s2[i].Trim());

                string[] s3 = s2[i].Trim().Split(new char[] { ' '}, StringSplitOptions.RemoveEmptyEntries);
                for (int j = 0; j < s3.Length; j++)
                {
                    Console.WriteLine(s3[j].Trim());
                }
            }

            //Console.WriteLine(ssp.Resolver.ByteToHexString(e.DatagramByte));
            ssp.SendData(s1);
        }

        static IPEndPoint m_lastUser;
        private static void WebSocket_ReceiveDataEnd(object sender, NetEventArgs e)
        {
            Console.WriteLine(e.Client.ID.ToString() + ",发送" + e.Client.Datagram);
        }

        private static void WebSocket_ClientClose(object sender, NetEventArgs e)
        {
            Console.WriteLine(e.Client.ID.ToString() + "已经断开");
        }

        private static void WebSocket_ClientConn(object sender, NetEventArgs e)
        {
            m_lastUser = (IPEndPoint)e.Client.ID;
            Console.WriteLine(e.Client.ID.ToString() + "已经链接");
        }

        private static void UdpClient_ReceiveDataEnd(object sender, NetEventArgs e)
        {
            throw new NotImplementedException();
        }

        private static void TcpClient_ReceiveDataEnd(object sender, NetEventArgs e)
        {
            throw new NotImplementedException();
        }

        private static void UdpServer_ReceiveDataEnd(object sender, NetEventArgs e)
        {
            throw new NotImplementedException();
        }

        private static void TcpServer_ReceiveDataEnd(object sender, NetEventArgs e)
        {
            throw new NotImplementedException();
        }

        private static void Sp_ReceiveDataEnd(object obj, SerialPortEventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}

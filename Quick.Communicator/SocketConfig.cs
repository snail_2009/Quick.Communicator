﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Quick.Communicator
{
    /// <summary>
    /// Socket 配置信息
    /// </summary>
    public class SocketConfig
    {
        /// <summary>
        /// 服务器IP地址
        /// 如果是Server模式, 该栏位不生效
        /// </summary>
        public string ServerIP { get; set; }
        /// <summary>
        /// 监听/链接服务器端口号
        /// </summary>
        public int Port { get; set; }
        /// <summary>
        /// 服务器地址
        /// </summary>
        public IPEndPoint ServerIPEndPoint
        {
            get
            {
                if (string.IsNullOrEmpty(ServerIP))
                {
                    throw new Exception("Server IP is empty");
                }
                return new IPEndPoint(IPAddress.Parse(ServerIP), Port);
            }
        }
        /// <summary>
        /// Client绑定本地Port
        /// 如果是Server模式, 该栏位不生效
        /// </summary>
        public int ClientLocalPort { get; set; }
        /// <summary>
        /// 绑定本地IP/Port
        /// </summary>
        public IPEndPoint LocalIPEndPoint
        {
            get
            {
                return new IPEndPoint(IPAddress.Any, ClientLocalPort);
            }
        }
        /// <summary>
        /// 字符编码 默认 ASCII
        /// </summary>
        public string Encoding { get; set; } = "ASCII";
        /// <summary>
        /// 报文结束符
        /// </summary>
        public string EndTag { get; set; }
        /// <summary>
        /// 报文结束符是否Hex
        /// </summary>
        public bool EndTagIsHex { get; set; } = false;
    }
}

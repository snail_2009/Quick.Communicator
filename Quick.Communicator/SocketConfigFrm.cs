﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Quick.Communicator
{
    public partial class SocketConfigFrm : Form
    {
        SocketConfig m_SocketConfig = null;
        public SocketConfigFrm(SocketConfig config = null)
        {
            m_SocketConfig = config;
            InitializeComponent();
            if (null != config)
            {
                txtConnectionIp.Text = config.ServerIP;
                nudPort.Value = config.Port;
                nudLocalPort.Value = config.ClientLocalPort;

                txtEndChar.Text = config.EndTag;

                chkIsHex.Checked = config.EndTagIsHex;

                cboEncoding.Text = config.Encoding;
            }
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (null == m_SocketConfig)
            {
                m_SocketConfig = new SocketConfig();
            }

            if (string.IsNullOrEmpty(txtConnectionIp.Text))
            {
                MessageBox.Show("请输入要链接的IP地址!", "提示");
                return;
            }
            if (0 == (int)nudPort.Value)
            {
                MessageBox.Show("请输入要链接的端口号!", "提示");
                return;
            }
            m_SocketConfig.ServerIP = txtConnectionIp.Text;
            m_SocketConfig.Port = (int)nudPort.Value;
            m_SocketConfig.ClientLocalPort = (int)nudLocalPort.Value;

            m_SocketConfig.Encoding = cboEncoding.Text;
            m_SocketConfig.EndTag = txtEndChar.Text?.Trim();
            m_SocketConfig.EndTagIsHex = chkIsHex.Checked;
            this.DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}

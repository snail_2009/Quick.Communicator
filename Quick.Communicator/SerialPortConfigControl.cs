﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;

namespace Quick.Communicator
{
    public partial class SerialPortConfigControl : UserControl
    {
        SerialConfig m_SerialConfig = null;
        public SerialPortConfigControl(SerialConfig config = null)
        {
            InitializeComponent();
            InitSetting();
            m_SerialConfig = config;
            SetConfig(config);
        }
        /// <summary>
        /// 初始化串口设定值
        /// </summary>
        private void InitSetting()
        {
            string[] ports = System.IO.Ports.SerialPort.GetPortNames();
            cboPortName.Items.AddRange(ports);

            List<string> baudRate = new List<string>();
            baudRate.Add("9600");
            baudRate.Add("14400");
            baudRate.Add("19200");
            baudRate.Add("38400");
            baudRate.Add("56000");
            baudRate.Add("57600");
            baudRate.Add("115200");
            baudRate.Add("128000");
            baudRate.Add("230400");
            baudRate.Add("256000");
            baudRate.Add("460800");
            baudRate.Add("500000");
            baudRate.Add("512000");
            cboBaudRate.DataSource = baudRate;

            List<int> DataBits = new List<int>() { 5, 6, 7, 8 };
            cboDataBits.DataSource = DataBits;
            cboDataBits.SelectedIndex = 3;

            List<string> parity = new List<string>();
            parity.Add("None");
            parity.Add("Odd");
            parity.Add("Even");
            parity.Add("Mark");
            parity.Add("Space");
            cboParity.DataSource = parity;
            List<string> stopBits = new List<string>();
            stopBits.Add("None");
            stopBits.Add("One");
            stopBits.Add("Two");
            stopBits.Add("OnePointFive");
            cboStopBits.DataSource = stopBits;
            cboStopBits.SelectedIndex = 1;
        }

        private void SetConfig(SerialConfig config)
        {
            if (null == config)
            {
                return;
            }
            cboPortName.Text = config.PortName;
            cboBaudRate.SelectedItem = config.BaudRate;
            cboDataBits.SelectedItem = config.DataBits;
            cboParity.SelectedIndex = Convert.ToInt16(config.Parity);
            cboStopBits.SelectedIndex = Convert.ToInt16(config.StopBits);
            chkDTR.Checked = config.DTR;
            chkRTS.Checked = config.RTS;
        }

        public SerialConfig GetConfig()
        {
            if (null == m_SerialConfig)
            {
                m_SerialConfig = new SerialConfig();
            }
            m_SerialConfig.PortName = cboPortName.Text;
            m_SerialConfig.BaudRate = Convert.ToInt32(cboBaudRate.SelectedItem);
            m_SerialConfig.DataBits = Convert.ToInt32(cboDataBits.SelectedItem);
            m_SerialConfig.Parity = (Parity)Convert.ToInt32(cboParity.SelectedIndex);
            m_SerialConfig.StopBits = (StopBits)Convert.ToInt32(cboStopBits.SelectedIndex);
            m_SerialConfig.DTR = chkDTR.Checked;
            m_SerialConfig.RTS = chkRTS.Checked;
            if (string.IsNullOrEmpty(m_SerialConfig.PortName))
            {
                MessageBox.Show("请选择端口号!");
                cboPortName.Focus();
                return null;
            }
            if ((int)m_SerialConfig.BaudRate == 0)
            {
                MessageBox.Show("请选择波特率!");
                cboBaudRate.Focus();
                return null;
            }
            if ((int)m_SerialConfig.DataBits == 0)
            {
                MessageBox.Show("请选择数据位!");
                cboDataBits.Focus();
                return null;
            }
            if (m_SerialConfig.StopBits == StopBits.None || (int)m_SerialConfig.StopBits == -1)
            {
                MessageBox.Show("请选择停止位!");
                cboStopBits.Focus();
                return null;
            }            
            if ((int)m_SerialConfig.Parity == -1)
            {
                MessageBox.Show("请选择奇偶校验位!");
                cboParity.Focus();
                return null;
            }
            return m_SerialConfig;
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            GetConfig();
            if (null == m_SerialConfig)
            {
                MessageBox.Show("请先进行串口配置!", "提示");
                return;
            }
            SuperSerialPort ssp = null;
            try
            {
                ssp = new SuperSerialPort(m_SerialConfig, new DatagramResolver(Encoding.ASCII));
                ssp.Start();
                MessageBox.Show("串口打开成功!", "提示");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "错误提示");
            }
            finally
            {
                ssp?.Stop();
                ssp?.Dispose();
                GC.Collect();
            }
        }
    }
}

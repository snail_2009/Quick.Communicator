﻿/*  CLR版本: 4.0.30319.18063
 * 系统时间: 2014/10/31 14:20:43
 * 创建年份: 2014
 *     作者: 程炜.Snail
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Quick.Communicator
{
    /// <summary>
    /// 数据报文分析器,通过分析接收到的原始数据,得到完整的数据报文.
    /// 继承该类可以实现自己的报文解析方法.
    /// 本类的现实的是标记符的方法,可以在继承类中实现其他的方法
    /// </summary>
    [Serializable]
    public class DatagramResolver : IResolver
    {
        /// <summary>
        /// 默认构造函数
        /// </summary>
        public DatagramResolver()
            : base()
        { }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="coder">字符编码</param>
        public DatagramResolver(string coder)
            : base(coder)
        { }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="coder">字符编码</param>
        public DatagramResolver(System.Text.Encoding coder)
            : base(coder)
        { }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="coder">字符编码</param>
        /// <param name="endTag">报文结束符</param>
        /// <param name="endTagIsHex">结束符是否是Hex格式</param>
        public DatagramResolver(string coder, string endTag, bool endTagIsHex = false)
            :base(coder,endTag,endTagIsHex)
        { }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="coder">字符编码</param>
        /// <param name="endTag">报文结束符</param>
        /// <param name="endTagIsHex">结束符是否是Hex格式</param>
        public DatagramResolver(System.Text.Encoding coder, string endTag, bool endTagIsHex = false)
            : base(coder, endTag, endTagIsHex)
        { }
    }
}

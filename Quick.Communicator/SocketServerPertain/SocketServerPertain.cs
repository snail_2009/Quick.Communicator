﻿/*  CLR版本: 4.0.30319.18063
 * 系统时间: 2014/10/31 14:20:43
 * 创建年份: 2014
 *     作者: 程炜.Snail
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Collections;
using System.Net;
using System.Diagnostics;
namespace Quick.Communicator
{
    /// <summary>
    /// Socket接收完信号信息后委托
    /// </summary>
    [Serializable]
    public delegate void ReceiveDataEndEventHandler(object sender, NetEventArgs e);
    /// <summary>
    /// Socket发送完信息后委托
    /// </summary>
    [Serializable]
    public delegate void SendDataEndEventHandler(object sender, SendStateEventArgs e);
    /// <summary>
    /// Socket 客户端连接改变
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    [Serializable]
    public delegate void ClientConnectionChangeEventHandler(object sender, ConnectCountEventArgs e);
    /// <summary>
    /// 服务器共接收数据量(byte)
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    [Serializable]
    public delegate void ReceiveTotalBytesEventHandler(object sender, ReceiveTotalBytesEventArgs e);
    /// <summary>
    /// 网络通讯事件模型委托
    /// </summary>
    [Serializable]
    public delegate void NetEventHandler(object sender, NetEventArgs e);
    /// <summary>
    /// 是否禁止的IP
    /// </summary>
    /// <param name="endPoint"></param>
    /// <returns></returns>
    [Serializable]
    public delegate bool IsBanIPEventHandler(EndPoint endPoint);
    /// <summary>
    /// 接收的字节总数
    /// </summary>
    public class ReceiveTotalBytesEventArgs : EventArgs
    {
        public ReceiveTotalBytesEventArgs(Int64 bytes)
        {
            this._TotalBytesRead = bytes;
        }
        private Int64 _TotalBytesRead;
        /// <summary>
        /// 一共接收的字节数
        /// </summary>
        public Int64 TotalBytesRead
        {
            get { return _TotalBytesRead; }
            set { _TotalBytesRead = value; }
        }
        /// <summary>
        /// 一共接收的字节数
        /// </summary>
        public double TotalKBRead
        {
            get { return Math.Round(_TotalBytesRead / 1024d, 2); }
        }
        /// <summary>
        /// 一共接收的字节数
        /// </summary>
        public double TotalMBRead
        {
            get { return Math.Round(_TotalBytesRead / 1048576d, 2); }
        }
        /// <summary>
        /// 一共接收的字节数
        /// </summary>
        public double TotalGBRead
        {
            get { return Math.Round(_TotalBytesRead / 1073741824d, 2); }
        }
        /// <summary>
        /// 需显示接收字节总数
        /// </summary>
        public string TotalReadShow
        {
            get
            {
                if (_TotalBytesRead > 1073741824)
                { //GB
                    return TotalGBRead + "GB";
                }
                else if (_TotalBytesRead > 1048576)
                { //MB
                    return TotalMBRead + "MB";
                }
                else if (_TotalBytesRead > 1024)
                { //KB
                    return TotalKBRead + "KB";
                }
                else
                {
                    return _TotalBytesRead + "Bytes";
                }
            }
        }
    }
    /// <summary>
    /// 这个类封装了一个单一的数据包,
    /// 无论是发送或接收的Socket都可以使用
    /// </summary>
    public class NetEventArgs : EventArgs
    {
        #region 字段
        /// <summary>
        /// 客户端与服务器之间的会话
        /// </summary>
        private Session _Client;
        #endregion

        #region 属性
        /// <summary>
        /// 获得激发该事件的会话对象
        /// </summary>
        public Session Client
        {
            get
            {
                return _Client;
            }
        }

        private ProtocolType _ProtocolType = ProtocolType.Unknown;
        /// <summary>
        /// 协议类型
        /// </summary>
        public ProtocolType ProtocolType
        {
            set { this._ProtocolType = value; }
            get
            {
                if (null != _Client && null != _Client.ClientSocketAsyncEventArgs)
                {
                    return ((Socket)_Client.ClientSocketAsyncEventArgs.UserToken).ProtocolType;
                }
                else
                {
                    return _ProtocolType;
                }
            }
        }
        #endregion

        #region 构造函数
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="client">客户端会话</param>
        public NetEventArgs(Session client)
        {
            if (null == client)
            {
                throw (new ArgumentNullException());
            }
            _Client = client;
        }
        #endregion

    }
    /// <summary>
    /// 数据发送状态
    /// </summary>
    public class SendStateEventArgs : EventArgs
    {
        private SendState _SendState;

        public SendState SendState
        {
            get { return _SendState; }
        }
        public SendStateEventArgs(SendState ss)
        {
            this._SendState = ss;
        }
    }
    /// <summary>
    /// 连接的客户端数量
    /// </summary>
    public class ConnectCountEventArgs : EventArgs
    {
        public ConnectCountEventArgs()
        { }
        public ConnectCountEventArgs(int count)
        {
            this._ConnCount = count;
        }
        private int _ConnCount;
        /// <summary>
        /// 当前连接的客户端数量
        /// </summary>
        public int ConnCount
        {
            get { return _ConnCount; }
            set { _ConnCount = value; }
        }
    }

    #region 发送指令状态
    /// <summary>
    /// 发送指令状态
    /// </summary>
    [Serializable]
    public enum SendState
    {
        /// <summary>
        /// 不成功
        /// </summary>
        Failure,
        /// <summary>
        /// 成功
        /// </summary>
        Success
    }
    #endregion
}

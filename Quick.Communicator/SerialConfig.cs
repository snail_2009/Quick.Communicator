﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Text;
//using System.Runtime.Serialization.Json;
using System.IO;

namespace Quick.Communicator
{
    /// <summary>
    /// 串口配置返回值
    /// </summary>
    public class SerialConfig
    {
        public SerialConfig()
        { }
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="strConfig"></param>
        //public SerialConfig(string strConfig)
        //{
        //    using (var mStream = new MemoryStream(Encoding.UTF8.GetBytes(strConfig)))
        //    {
        //        var serializer = new DataContractJsonSerializer(typeof(SerialConfig));
        //        SerialConfig readConfig = (SerialConfig)serializer.ReadObject(mStream);
        //        this.PortName = readConfig.PortName;
        //        this.BaudRate = readConfig.BaudRate;
        //        this.Parity = readConfig.Parity;
        //        this.DataBits = readConfig.DataBits;
        //        this.StopBits = readConfig.StopBits;
        //        this.DTR = readConfig.DTR;
        //        this.RTS = readConfig.RTS;
        //    }
        //}
        /// <summary>
        /// COM 端口
        /// </summary>
        public string PortName { get; set; }
        /// <summary>
        /// 波特率
        /// </summary>
        public int BaudRate { get; set; }
        /// <summary>
        /// 奇偶校验位
        /// </summary>
        public Parity Parity { get; set; }
        /// <summary>
        /// 奇偶校验位
        /// </summary>
        public int DataBits { get; set; }
        /// <summary>
        /// 停止位
        /// </summary>
        public StopBits StopBits { get; set; }
        /// <summary>
        /// DTR
        /// </summary>
        public bool DTR { get; set; }
        /// <summary>
        /// RTS
        /// </summary>
        public bool RTS { get; set; }

        //public override string ToString()
        //{
        //    DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(SerialConfig));
        //    string dataString = "";
        //    using (var stream = new MemoryStream())
        //    {
        //        serializer.WriteObject(stream, this);
        //        byte[] dataBytes = new byte[stream.Length];
        //        stream.Position = 0;
        //        stream.Read(dataBytes, 0, (int)stream.Length);
        //        dataString = Encoding.UTF8.GetString(dataBytes);
        //    }
        //    return dataString;
        //}
    }
}

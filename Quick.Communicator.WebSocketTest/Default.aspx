﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Quick.Communicator.WebSocketTest._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script src="~/Scripts/jquery-1.10.2.min.js"></script>
    <script>
    $(function ()
    {
        $("#sendM").click(function ()
        {
            sendMsg();
        });
        $("#closeWebSocket").click(function ()
        {
            close();
        });
    });
    var ws = null;
    if ('WebSocket' in window) {
        ws = new WebSocket("ws://localhost:5555/websocket");
    }
    else {
        alert('当前浏览器 Not support websocket')
    }
    ws.onopen = function (evt)
    {
        console.log("Connection open ...");
    };
    ws.onmessage = function (evt)
    {
        console.log("Received Message: " + evt.data);
    };
    ws.onclose = function (evt)
    {
        console.log("Connection closed.");        
    };
    function close()
    {
        ws.close();
    }
    function sendMsg()
    {
        ws.send($("#content").val())
    }
</script>

发送内容：<input type="text" id="content" name="content" />
<input id="sendM" type="button" value="发送" />
<input id="closeWebSocket" type="button" value="关闭" />
</asp:Content>

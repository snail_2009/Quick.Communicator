﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Quick.Communicator.WebSocketTest.Startup))]
namespace Quick.Communicator.WebSocketTest
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}

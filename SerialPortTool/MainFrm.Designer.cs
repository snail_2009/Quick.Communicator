﻿namespace SerialPortTool
{
    partial class MainFrm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.chkLog = new System.Windows.Forms.CheckBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.serialControl1 = new SerialPortTool.SerialControl();
            this.serialControl2 = new SerialPortTool.SerialControl();
            this.chkTransfer = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkLog
            // 
            this.chkLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkLog.AutoSize = true;
            this.chkLog.Checked = true;
            this.chkLog.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLog.Location = new System.Drawing.Point(15, 381);
            this.chkLog.Name = "chkLog";
            this.chkLog.Size = new System.Drawing.Size(108, 16);
            this.chkLog.TabIndex = 3;
            this.chkLog.Text = "记录日志到文件";
            this.chkLog.UseVisualStyleBackColor = true;
            this.chkLog.CheckedChanged += new System.EventHandler(this.chkLog_CheckedChanged);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(1, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.serialControl1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.serialControl2);
            this.splitContainer1.Size = new System.Drawing.Size(1050, 375);
            this.splitContainer1.SplitterDistance = 525;
            this.splitContainer1.TabIndex = 4;
            // 
            // serialControl1
            // 
            this.serialControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.serialControl1.Location = new System.Drawing.Point(0, 0);
            this.serialControl1.Name = "serialControl1";
            this.serialControl1.Size = new System.Drawing.Size(525, 375);
            this.serialControl1.TabIndex = 2;
            // 
            // serialControl2
            // 
            this.serialControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.serialControl2.Location = new System.Drawing.Point(0, 0);
            this.serialControl2.Name = "serialControl2";
            this.serialControl2.Size = new System.Drawing.Size(521, 375);
            this.serialControl2.TabIndex = 1;
            // 
            // chkTransfer
            // 
            this.chkTransfer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkTransfer.AutoSize = true;
            this.chkTransfer.Checked = true;
            this.chkTransfer.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTransfer.Location = new System.Drawing.Point(187, 381);
            this.chkTransfer.Name = "chkTransfer";
            this.chkTransfer.Size = new System.Drawing.Size(120, 16);
            this.chkTransfer.TabIndex = 5;
            this.chkTransfer.Text = "串口调试转发模式";
            this.chkTransfer.UseVisualStyleBackColor = true;
            this.chkTransfer.CheckedChanged += new System.EventHandler(this.chkTransfer_CheckedChanged);
            // 
            // MainFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1050, 407);
            this.Controls.Add(this.chkTransfer);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.chkLog);
            this.Name = "MainFrm";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainFrm_FormClosing);
            this.Load += new System.EventHandler(this.MainFrm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private SerialControl serialControl2;
        private SerialControl serialControl1;
        private System.Windows.Forms.CheckBox chkLog;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.CheckBox chkTransfer;
    }
}


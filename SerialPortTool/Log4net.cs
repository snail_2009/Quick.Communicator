﻿/*CLR Version: 4.0.30319.18063
 * Creat Date: 2016/6/27 10:26:02
 * Creat Year: 2016
 * Creator: 程炜.Snail
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;
using System.Xml;
using System.Threading;
using System.Configuration;
namespace Utility.Log
{
    /// <summary>
    /// Log4net log日志记录
    /// </summary>
    public class Log4net
    {
        private static Log4net instance;
        private static object syncRoot = new Object();
        private Dictionary<string, log4net.ILog> m_LogManagers = new Dictionary<string, log4net.ILog>();
        /// <summary>
        /// 压缩文件线程
        /// </summary>
        private Thread m_CompressFileThread;
        private bool m_IsCompress = true;
        private Log4net()
        {
            try
            {
                string log4netConfigPath = string.Empty;
                {
                    log4netConfigPath = Path.Combine(Application.StartupPath, "log4net.config");
                }
                if (File.Exists(log4netConfigPath))
                {
                    //日志配置文件加载
                    log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo(log4netConfigPath));
                    XmlNodeList xmlNodeList = XML.XMLHelper.GetXmlNodeListByXpath(log4netConfigPath, "//configuration//log4net//logger");
                    if (xmlNodeList != null && xmlNodeList.Count > 0)
                    {
                        for (int i = 0; i < xmlNodeList.Count; i++)
                        {
                            string logName = xmlNodeList[i].Attributes["name"].Value;
                            m_LogManagers.Add(logName.ToUpper(), log4net.LogManager.GetLogger(logName));
                        }
                    }
                }
            }
            catch (Exception ex)
            { }
        }
        /// <summary>
        /// Process Logger Data,
        /// 默认开启 /Log/ 路径的日志压缩功能.如需自定义设置日志路径,使用 SetCompressLogFiles 方法进行修改
        /// </summary>
        public static Log4net Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                        {
                            instance = new Log4net();
                        }
                    }
                }
                return instance;
            }
        }
        /// <summary>
        /// 获取LOG对象
        /// </summary>
        /// <param name="logName"></param>
        /// <returns></returns>
        public log4net.ILog this[string logName]
        {
            get
            {
                log4net.ILog log = null;
                m_LogManagers.TryGetValue(logName.ToUpper(), out log);
                if (null == log)
                {
                    log = log4net.LogManager.GetLogger("Default");//提供一个默认值,防止log没有配置,导致程序异常
                }
                return log;
            }
        }        
    }
}
/* 代码自动创建Log对象

log4net.Appender.RollingFileAppender appender = new log4net.Appender.RollingFileAppender();
appender.Name = this.GetType().Name;
appender.File = string.Concat("Log/PushData/", this.CustomerCode, "/", this.GetType().Name, "/");
appender.StaticLogFileName = false;
appender.AppendToFile = true;
appender.DatePattern = "yyyyMMdd\".log\"";
appender.RollingStyle = log4net.Appender.RollingFileAppender.RollingMode.Composite;            
appender.LockingModel = new FileAppender.MinimalLock(); //不占用日志文件进程-,允许多线程同时写入
appender.Layout = new log4net.Layout.PatternLayout("%n日志时间：%d [%t] %n日志级别：%-5p %n%m %n");
appender.MaximumFileSize = "100MB";
appender.MaxSizeRollBackups = 1000;
appender.Encoding = System.Text.Encoding.UTF8;
appender.ActivateOptions();

log4net.Repository.ILoggerRepository repository = log4net.LogManager.CreateRepository(this.GetType().Name);
log4net.Config.BasicConfigurator.Configure(repository, appender);

Log = log4net.LogManager.GetLogger(repository.Name, this.GetType().Name);
*/

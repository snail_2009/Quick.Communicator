﻿/*CLR Version: 4.0.30319.18063
 * Creat Date: 2016/6/27 10:26:02
 * Creat Year: 2016
 * Creator: 程炜.Snail
 */
using System;
using System.Collections.Generic;
using System.Text;

namespace Utility.Byte
{
    /// <summary>
    /// 提供 Byte 与 HexString 互转功能
    /// </summary>
    public static class HexString
    {
        /// <summary>
        /// 将字节数据转换成信号协议中描述的整形数据
        /// </summary>
        /// <param name="bt"></param>
        /// <returns></returns>
        public static Int64 ByteConvertInt(byte[] bt)
        {
            int n = bt.Length;
            string[] str = new string[n];
            for (int i = 0; i < n; i++)
            {
                str[i] = bt[i].ToString("X2");
            }
            string intStr = string.Concat(str);
            return Int64.Parse(intStr, System.Globalization.NumberStyles.HexNumber);
        }
        /// <summary>
        /// 将整数按照固定的位数转换为整数组成字符串
        /// </summary>
        /// <param name="number"></param>
        /// <param name="bitArray"> 从高位到低位</param>
        /// <returns></returns>
        public static string IntToBitIntString(long number, int[] bitArray)
        {
            StringBuilder str = new StringBuilder();
            Int64 converInt = 0;
            for (int i = bitArray.Length - 1; i >= 0; i--)
            {
                converInt = number & (Convert.ToInt64((Math.Pow(2, bitArray[i]) - 1)));
                number = number >> Convert.ToInt32(bitArray[i]);
                str.Insert(0, converInt.ToString("D2"));
            }
            return str.ToString();
        }
        /// <summary> 
        /// 字节数组转16进制字符串
        /// 如 01 02 A3 C4 B5 06 07(大写英文字母)
        /// </summary> 
        /// <param name="bytes"></param>
        /// <param name="splicStr"></param>
        /// <returns></returns> 
        public static string ByteToHexString(byte[] bytes, string splicStr = " ")
        {
            System.Text.StringBuilder strBuilder = new System.Text.StringBuilder();
            if (bytes != null)
            {
                for (int i = 0; i < bytes.Length; i++)
                {
                    if (strBuilder.Length > 0)
                    {
                        strBuilder.Append(splicStr);
                    }
                    strBuilder.Append(bytes[i].ToString("X2"));
                }
            }
            return strBuilder.ToString();
        }
        /// <summary> 
        /// 字符串转16进制字节数组
        /// </summary> 
        /// <param name="hexString"></param> 
        /// <param name="splicStr"></param>
        /// <returns></returns> 
        public static byte[] HexStringToByte(string hexString, string splicStr = " ")
        {
            if (!string.IsNullOrEmpty(splicStr))
            {
                hexString = hexString.Replace(splicStr, "");
            }
            if ((hexString.Length % 2) != 0)
            {
                hexString = string.Concat("0", hexString);
            }
            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
            {
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2).Trim(), 16);
            }
            return returnBytes;
        }
        /// <summary>
        /// 16进制转10进制
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static Int32 HexString2Int(this string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return 0;
            }
            s = s.Trim();
            return Int32.Parse(s, System.Globalization.NumberStyles.HexNumber);
        }
        /// <summary>
        /// 十进制转十六进制
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        public static string Int2HexString(this int d)
        {
            string x = "";
            if (d < 16)
            {
                x = Change(d);
            }
            else
            {
                int c;
                int s = 0;
                int n = d;
                int temp = d;
                while (n >= 16)
                {
                    s++;
                    n = n / 16;
                }
                string[] m = new string[s];
                int i = 0;
                do
                {
                    c = d / 16;
                    m[i++] = Change(d % 16);//判断是否大于10，如果大于10，则转换为A~F的格式
                    d = c;
                } while (c >= 16);
                x = Change(d);
                for (int j = m.Length - 1; j >= 0; j--)
                {
                    x += m[j];
                }
            }
            return x;
        }

        //判断是否为10~15之间的数，如果是则进行转换
        private static string Change(int d)
        {
            string x = "";
            switch (d)
            {
                case 10:
                    x = "A";
                    break;
                case 11:
                    x = "B";
                    break;
                case 12:
                    x = "C";
                    break;
                case 13:
                    x = "D";
                    break;
                case 14:
                    x = "E";
                    break;
                case 15:
                    x = "F";
                    break;
                default:
                    x = d.ToString();
                    break;
            }
            return x;
        }
    }
}
